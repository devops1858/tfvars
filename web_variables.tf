### ALB Module Vars ###

variable "web_create_lb" {
  description = "Controls if the Load Balancer should be created"
  type        = bool
  default     = true
}

variable "web_alb_drop_invalid_header_fields" {
  description = "Indicates whether invalid header fields are dropped in application load balancers. Defaults to false."
  type        = bool
  default     = false
}

variable "web_alb_enable_deletion_protection" {
  description = "If true, deletion of the load balancer will be disabled via the AWS API. This will prevent Terraform from deleting the load balancer. Defaults to false."
  type        = bool
  default     = false
}

variable "web_alb_enable_http2" {
  description = "Indicates whether HTTP/2 is enabled in application load balancers."
  type        = bool
  default     = true
}

variable "web_alb_enable_cross_zone_load_balancing" {
  description = "Indicates whether cross zone load balancing should be enabled in application load balancers."
  type        = bool
  default     = false
}

variable "web_alb_https_listeners" {
  description = "A list of maps describing the HTTPS listeners for this alb. Required key/values: port, certificate_arn. Optional key/values: ssl_policy (defaults to ELBSecurityPolicy-TLS-1-2-2017-01), target_group_index (defaults to https_listeners[count.index])"
  type        = any
  default     = []
}

variable "web_alb_idle_timeout" {
  description = "The time in seconds that the connection is allowed to be idle."
  type        = number
  default     = 60
}

variable "web_alb_ip_address_type" {
  description = "The type of IP addresses used by the subnets for your load balancer. The possible values are ipv4 and dualstack."
  type        = string
  default     = "ipv4"
}

variable "web_alb_internal" {
  description = "Boolean determining if the load balancer is internal or externally facing."
  type        = bool
  default     = false
}

variable "web_alb_load_balancer_create_timeout" {
  description = "Timeout value when creating the alb."
  type        = string
  default     = "10m"
}

variable "web_alb_load_balancer_delete_timeout" {
  description = "Timeout value when deleting the alb."
  type        = string
  default     = "10m"
}

variable "web_alb_name_prefix" {
  description = "The resource name prefix and Name tag of the load balancer."
  type        = string
  default     = null
}

variable "web_alb_name" {
  description = "The resource name prefix and Name tag of the load balancer."
  type        = string
  default     = null
}

variable "web_alb_app_type" {
  description = "type of application used for naming such as api or web"
  type        = string
  default     = null
}

variable "web_alb_load_balancer_type" {
  description = "The type of load balancer to create. Possible values are application or network."
  type        = string
  default     = "application"
}

variable "web_alb_load_balancer_update_timeout" {
  description = "Timeout value when updating the alb."
  type        = string
  default     = "10m"
}

variable "web_alb_access_logs" {
  description = "Map containing access logging configuration for load balancer."
  type        = map(string)
  default     = {}
}

variable "web_alb_subnet_mapping" {
  description = "A list of subnet mapping blocks describing subnets to attach to network load balancer"
  type        = list(map(string))
  default     = []
}

variable "web_alb_target_groups" {
  description = "A list of maps containing key/value pairs that define the target groups to be created. Order of these maps is important and the index of these are to be referenced in listener definitions. Required key/values: name, backend_protocol, backend_port"
  type        = any
  default     = []
}

variable "web_alb_security_groups" {
  type        = list
  default     = null
}


### ASG Module Vars ###

variable "web_ServerType" {
  description = "Server Type to give remote access to server."
  type        = string
  default     = ""
}

variable "web_volume_size" {
  description = "volume_size of the ebs."
  type        = string
  default     = ""
}

variable "web_bootstrap_file_path" {
  description = "file path to bootstrap template"
  type        = string
}

variable "web_asg_enabled" {
  type        = bool
  description = "Whether to create the resources. Set to `false` to prevent the module from creating any resources"
  default     = true
}

## Cloudwatch Metric ##

variable "web_cpu_utilization_high_period_seconds" {
  type    = string
}

variable "web_cpu_utilization_high_statistic" {
  type    = string
}

variable "web_cpu_utilization_high_threshold_percent" {
  type    = string
}

variable "web_cpu_utilization_high_evaluation_periods" {
  type    = string
}

variable "web_cpu_utilization_low_evaluation_periods" {
  type    = string
}

variable "web_cpu_utilization_low_period_seconds" {
  type    = string
}

variable "web_cpu_utilization_low_statistic" {
  type    = string
}

variable "web_cpu_utilization_low_threshold_percent" {
  type    = string
}

variable "web_scaling_adjustment_type" {
  description = "scaling_adjustment_type"
  type        = string
}

variable "web_scaling_policy_type" {
  description = "scaling_policy_type"
  type        = string
}

variable "web_scaling_adjustment_add" {
  description = "scaling_adjustment"
  type        = string
}

variable "web_scaling_adjustment_sub" {
  description = "scaling_adjustment"
  type        = string
}

variable "web_cpu_utilization_high_comparison_operator" {
  description = "comparison_operator"
  type        = string
}

variable "web_cpu_metric_name" {
  description = "metric_name"
  type        = string
}

variable "web_cpu_namespace" {
  description = "namespace"
  type        = string
}

variable "web_cpu_utilization_low_comparison_operator" {
  description = "low_comparison_operator"
  type        = string
}

## EC2 Instance Vars ##
variable "web_create_lt" {
  description = "Whether to create launch template"
  type        = bool
  default     = true
}

variable "web_security_groups" {
  type        = list
  default     = null
}

variable "web_block_device_mappings" {
  description = "Additional EBS block devices to attach to the instance"
  type        = list(map(string))
  default     = []
}

variable "web_ebs_optimized" {
  description = "If true, the launched EC2 instance will be EBS-optimized"
  type        = bool
  default     = false
}

variable "web_capacity_rebalance" {
  description = "Indicates whether capacity rebalance is enabled. Otherwise, capacity rebalance is disabled"
  type        = string
  default     = "false"
}

variable "web_create_asg" {
  description = "Whether to create autoscaling group"
  type        = bool
  default     = true
}

variable "web_create_asg_with_initial_lifecycle_hook" {
  description = "Create an ASG with initial lifecycle hook"
  type        = bool
  default     = false
}

variable "web_instance_refresh" {
  description = "The instance refresh definition"
  type = object({
    strategy = string
    preferences = object({
      instance_warmup        = number
      min_healthy_percentage = number
    })
    triggers = list(string)
  })

  default = null
}

variable web_mixed_instances_policy {
  description = "policy to used mixed group of on demand/spot of differing types. Launch template is automatically generated. https://www.terraform.io/docs/providers/aws/r/autoscaling_group.html#mixed_instances_policy-1"

  type = object({
    instances_distribution = object({
      on_demand_allocation_strategy            = string
      on_demand_base_capacity                  = number
      on_demand_percentage_above_base_capacity = number
      spot_allocation_strategy                 = string
      spot_instance_pools                      = number
      spot_max_price                           = string
    })
    override = list(object({
      instance_type     = string
      weighted_capacity = number
    }))
  })
  default = null
}

variable "web_min_size" {
  description = "web_scaling_group_min_size"
  type        = string
}

variable "web_max_size" {
  description = "web_scaling_group_max_size"
  type        = string
}

variable "web_desired_capacity" {
  description = "The number of Amazon EC2 instances that should be running in the group"
  type        = string
}

variable "web_health_check_type" {
  description = "Controls how health checking is done. Values are - EC2 and ELB"
  type        = string
}

variable "web_health_check_grace_period" {
  description = "Controls the grace period before autoscaling starts the health checks"
  type        = string
}

variable "web_initial_lifecycle_hook_default_result" {
  description = "Defines the action the Auto Scaling group should take when the lifecycle hook timeout elapses or if an unexpected failure occurs. The value for this parameter can be either CONTINUE or ABANDON"
  type        = string
  default     = "ABANDON"
}

variable "web_initial_lifecycle_hook_lifecycle_transition" {
  description = "The instance state to which you want to attach the initial lifecycle hook"
  type        = string
  default     = "autoscaling:EC2_INSTANCE_TERMINATING"
}

variable "web_initial_lifecycle_hook_heartbeat_timeout" {
  description = "Defines the amount of time, in seconds, that can elapse before the lifecycle hook times out. When the lifecycle hook times out, Auto Scaling performs the action defined in the DefaultResult parameter"
  type        = string
  default     = "360"
}

variable "web_instance_type" {
  description = "instance type"
  type        = string
}

variable "web_host_name" {
  description = "Name of the host header"
  type        = string
  default     = null
}