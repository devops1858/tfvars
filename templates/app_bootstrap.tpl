#!/bin/bash
set -x
exec > >(tee /var/log/user-data-instance.log|logger -t user-data ) 2>&1

# Sleep to ensure all agents are installed first and AD cache is cleared.
while [ ! -f /etc/sysconfig/cfg-agents-installed ]; do sleep 10; done
adflush -a

# Changing permissions on key files to meet CIS
find /var/log -type f -exec chmod g-wx,o-rwx {} +