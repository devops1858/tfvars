### Latest AMI look up
data "aws_ami" "linux_ami" {
  most_recent = true
  #owners      = [data.aws_caller_identity.current.account_id]
  owners = ["793022030544"]   # Use Shared AMI, instead of account specific AMI (commented out above)
  name_regex  = "RHEL7_.?-CFG-AMI-*"
  tags = {
    Name = "RHEL 7.9 N"
  }
  filter {
    name   = "block-device-mapping.encrypted"
    values = ["true"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

### Ingress VPC Web Subnets
data "aws_subnet_ids" "web_subnets_ingress" {
  vpc_id = var.vpc_id_ingress

  filter {
    name   = "tag:Name"
    values = ["*-web-*"]
  }
}

### Internal VPC Web Subnets
data "aws_subnet_ids" "web_subnets_internal" {
  vpc_id = var.vpc_id_internal

  filter {
    name   = "tag:Name"
    values = ["*-web-*"]
  }
}

### Internal VPC App Subnets
data "aws_subnet_ids" "app_subnets_internal" {
  vpc_id = var.vpc_id_internal

  filter {
    name   = "tag:Name"
    values = ["*-app-*"]
  }
}

### Internal VPC Mgmt Subnets
data "aws_subnet_ids" "mgmt_subnets_internal" {
  vpc_id = var.vpc_id_internal

  filter {
    name   = "tag:Name"
    values = ["*-mgmt-*"]
  }
}

### Foundational SGs

#Ingress VPC linux-sg
data "aws_security_groups" "linux_sg_ingress" {
  filter {
    name   = "group-name"
    values = ["linux-sg"]
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id_ingress]
  }
}

#Ingress VPC allow_tls
data "aws_security_groups" "allow_tls_ingress" { 
  filter {
    name   = "group-name"
    values = ["allow_tls"]
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id_ingress]
  }
}

#Internal VPC linux-sg
data "aws_security_groups" "linux_sg_internal" {
  filter {
    name   = "group-name"
    values = ["linux-sg"]
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id_internal]
  }
}

#Internal VPC allow_tls
data "aws_security_groups" "allow_tls_internal" { 
  filter {
    name   = "group-name"
    values = ["allow_tls"]
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id_internal]
  }
}

#Application Ingress VPC SGs
data "aws_security_groups" "sg_WebLB" { 
  filter {
    name   = "group-name"
    values = ["*-sg-WebLB"]
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id_ingress]
  }

  filter {
    name   = "tag:ApplicationID"
    values = ["${lookup(var.mandatory_tags, "ApplicationID")}"]
  }
}

data "aws_security_groups" "sg_WebServers" { 
  filter {
    name   = "group-name"
    values = ["*-sg-WebServers"]
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id_ingress]
  }

  filter {
    name   = "tag:ApplicationID"
    values = ["${lookup(var.mandatory_tags, "ApplicationID")}"]
  }
}

#Application Internal VPC SGs
data "aws_security_groups" "sg_AppLB" { 
  filter {
    name   = "group-name"
    values = ["*-sg-AppLB"]
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id_internal]
  }

  filter {
    name   = "tag:ApplicationID"
    values = ["${lookup(var.mandatory_tags, "ApplicationID")}"]
  }
}

data "aws_security_groups" "sg_AppServers" { 
  filter {
    name   = "group-name"
    values = ["*-sg-AppServers"]
  }

  filter {
    name   = "vpc-id"
    values = [var.vpc_id_internal]
  }

  filter {
    name   = "tag:ApplicationID"
    values = ["${lookup(var.mandatory_tags, "ApplicationID")}"]
  }
}