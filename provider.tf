variable vault_token {}

provider "vault" {
  address         = "https://vault-cldsvc-prod.corp.internal.citizensbank.com/"
  skip_tls_verify = true
  token           = var.vault_token
}

data "vault_aws_access_credentials" "creds" {
  backend = "aws"
  role    = var.vault_role_name
  type    = "sts"
}

provider "aws" {
  region      = var.region
  access_key  = data.vault_aws_access_credentials.creds.access_key
  secret_key  = data.vault_aws_access_credentials.creds.secret_key
  token       = data.vault_aws_access_credentials.creds.security_token
}

data "aws_iam_account_alias"  "current" {}
data "aws_caller_identity"    "current" {}