terraform {
  backend "remote" {
    hostname     = "terraform-cldsvc-prod.corp.internal.citizensbank.com"
    organization = "cfg-cloud-services"

    workspaces {
      prefix = "eds-app-"
    }
  }
}