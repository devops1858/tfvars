def workspace = "${TFE_WORKSPACE}"

def performAction() {
  try {
    sh """
       echo 'Performing Terraform13 ${ACTION}'
        terraform13 ${ACTION} --auto-approve -no-color
       """
  } catch (e) {
    sh """
       echo 'Performing Terraform13 ${ACTION} Again'
       terraform13 ${ACTION} --auto-approve -no-color
       """
  }
}

pipeline {
  agent any

  options {
    disableConcurrentBuilds()
  }

  stages {
    stage("Configuring Autovars") {
      steps {
        sh """
           rm -rf ${WORKSPACE}/environments/*.auto.tfvars
           rm -rf ${WORKSPACE}/*.auto.tfvars
           cp ${WORKSPACE}/environments/${TFE_WORKSPACE}.tfvars ${WORKSPACE}/${TFE_WORKSPACE}.auto.tfvars
           """
      }
    }
    stage('Initializing Terraform') {
      steps {
        sh """
           echo 'Using TFE Workspace: ${TFE_WORKSPACE}'
           set +x
           echo -e "vault_token=\\"`vault login -token-only -tls-skip-verify -address="https://vault-cldsvc-prod.corp.internal.citizensbank.com/" -method=aws role=jenkins-aws-role`\\"" > token.auto.tfvars
           set -x
           echo 'Performing Terraform13 Init'
           terraform13 init -no-color -input=false || true
           terraform13 workspace select ${TFE_WORKSPACE}
           """
      }
    }
    stage('Running Terraform Plan') {
      steps {
        sh """
           if [ ${ACTION} == 'destroy' ]; then
           echo 'Performing Terraform13 Destroy Plan'
           terraform13 plan -destroy -no-color 
           elif [ ${ACTION} == 'apply' ]; then
           echo 'Performing Terraform13 Apply Plan'
           terraform13 plan -no-color
           fi
           """
      }
    }
    stage("Apply Confirmation") {
      steps{
        input 'Proceed to apply?'
      }
    }
    stage('Running Terraform Apply') {
      steps {
        script {
          performAction()
        }
      }
    }
  }
  post {
    always {
      echo "Cleaning up directory"
      // deleteDir() // Uncomment this if you need to clean your workspace
    }
  } // post ends
}