module "label" {
  source  = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/label-module/cfg"
  version = "2.1.7"

  name            = var.app_name
  environment     = var.app_environment
  mandatory_tags  = var.mandatory_tags
  attributes      = var.attributes
  additional_tags = var.additional_tags
  snowapi_disable = var.snowapi_disable
}