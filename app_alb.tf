module "app_alb" {
  source  = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/alb-module/aws"
  version = "2.0.9"

  load_balancer_type    = var.app_alb_load_balancer_type
  vpc_id                = var.vpc_id_internal
  subnets               = data.aws_subnet_ids.app_subnets_internal.ids
  security_groups       = concat(data.aws_security_groups.sg_AppLB.ids, data.aws_security_groups.allow_tls_internal.ids)
  access_logs           = var.app_alb_access_logs
  target_groups         = var.app_alb_target_groups
  https_listeners       = var.app_alb_https_listeners
  internal              = var.app_alb_internal
  tags                  = module.label.tags
  name                  = lower("${module.label.name}-${module.label.environment}-${var.region}-alb-app")
}