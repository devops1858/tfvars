### LABEL MODULE VARS ###

variable "app_name" {
  type        = string
  description = "Resource name according to the Cloud Naming Conventions"
}

variable "app_environment" {
  type        = string
  description = "Stage, e.g. 'DEV', 'SIT', 'QA', 'PROD', 'DR'"
}

variable "mandatory_tags" {
  type        = map(string)
  description = "Map of mandatory tags according to the Cloud Tagging Standard, excepting Name, Environment and BunsinessUnit"
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `department`, `stage`, `name` and `attributes`"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "List of additional attributes (e.g. [`global`])"
}

variable "additional_tags" {
  type        = map(string)
  default     = {}
  description = "Map of additional tags (e.g. `map('foo','bar')`"
}

variable "snowapi_disable" {
  type        = bool
  default     = false
}


### Vault Role Name ###

variable "vault_role_name" {
  description = "Enter Vault Role name"
  type        = string
}


### Region ###

variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}


### EC2 Vars ###
variable "iam_instance_profile" {
  description = "The IAM instance profile to associate with launched instances"
  type        = string
  default     = ""
}

variable "key_name" {
  description = "Key Name"
  type        = string
}

variable "kms_key_id" {
  description = "kms_key_id of the KMS for encryption"
  type        = string
  default     = ""
}


### VPCs ###

variable "vpc_id_internal" {
  description = "VPC id of Internal VPC."
  type        = string
  default     = null
}

variable "vpc_id_ingress" {
  description = "VPC id of Ingress VPC."
  type        = string
  default     = null
}