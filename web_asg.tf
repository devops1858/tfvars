data "template_file" "web_bootstrap" {
  template = file(var.web_bootstrap_file_path)
}

module "web_asg" {
  source                    = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/autoscaling-module/aws"
  version                   = "2.0.7"
  create_lt                 = var.web_create_lt
  security_groups           = concat(data.aws_security_groups.sg_WebServers.ids, data.aws_security_groups.linux_sg_ingress.ids)
  name                      = lower("${module.label.name}-${module.label.environment}-${var.region}-asg-web")
  block_device_mappings     = var.web_block_device_mappings
  iam_instance_profile      = var.iam_instance_profile
  image_id                  = data.aws_ami.linux_ami.id
  ebs_optimized             = var.web_ebs_optimized
  capacity_rebalance        = var.web_capacity_rebalance
  create_asg                = var.web_create_asg
  instance_refresh          = var.web_instance_refresh
  mixed_instances_policy    = var.web_mixed_instances_policy
  min_size                  = var.web_min_size
  max_size                  = var.web_max_size
  desired_capacity          = var.web_desired_capacity
  vpc_zone_identifier       = data.aws_subnet_ids.web_subnets_ingress.ids
  target_group_arns         = [module.web_alb.target_group_arns[0]]
  health_check_type         = var.web_health_check_type
  health_check_grace_period = var.web_health_check_grace_period
  tags_as_map               = merge(module.label.tags, {"Name" = "${module.label.name}-${lookup(var.mandatory_tags, "ApplicationID")}-${module.label.environment}-${var.region}-instance-web"})
  instance_type             = var.web_instance_type
  key_name                  = var.key_name
  user_data_base64          = base64encode(data.template_file.web_bootstrap.rendered)
}

resource "aws_autoscaling_lifecycle_hook" "web_hook" {
  count                  = var.web_create_asg ? 1 : 0
  name                   = lower("${module.label.name}-${module.label.environment}-${var.region}-lifecycle-hook")
  autoscaling_group_name = module.web_asg.autoscaling_group_name
  default_result         = var.web_initial_lifecycle_hook_default_result
  lifecycle_transition   = var.web_initial_lifecycle_hook_lifecycle_transition
  heartbeat_timeout      = var.web_initial_lifecycle_hook_heartbeat_timeout
}

resource "aws_autoscaling_policy" "web_autoscaling_group_scale_up_policy" {
  count                   = var.web_asg_enabled ? 1 : 0
  name                    = lower("${module.label.name}-${module.label.environment}-${var.region}-asg-scaleup-policy-web")
  adjustment_type         = var.web_scaling_adjustment_type
  policy_type             = var.web_scaling_policy_type
  scaling_adjustment      = var.web_scaling_adjustment_add
  autoscaling_group_name  = module.web_asg.autoscaling_group_name
}

resource "aws_autoscaling_policy" "web_autoscaling_group_scale_down_policy" {
  count                   = var.web_asg_enabled ? 1 : 0
  name       		          = lower("${module.label.name}-${module.label.environment}-${var.region}-asg-scaledown-policy-web")
  adjustment_type         = var.web_scaling_adjustment_type
  policy_type             = var.web_scaling_policy_type
  scaling_adjustment      = var.web_scaling_adjustment_sub
  autoscaling_group_name  = module.web_asg.autoscaling_group_name
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_high" {
  count               = var.web_asg_enabled ? 1 : 0
  alarm_name          = lower("${module.label.name}-${module.label.environment}-${var.region}-asg-alarm-cpuhigh-web")
  comparison_operator = var.web_cpu_utilization_high_comparison_operator 
  evaluation_periods  = var.web_cpu_utilization_high_evaluation_periods
  metric_name         = var.web_cpu_metric_name 
  namespace           = var.web_cpu_namespace 
  period              = var.web_cpu_utilization_high_period_seconds
  statistic           = var.web_cpu_utilization_high_statistic
  threshold           = var.web_cpu_utilization_high_threshold_percent

  dimensions = {
    AutoScalingGroupName = module.web_asg.autoscaling_group_name
  }

  alarm_description  = "Alarm if CPU Utilization is above ${var.web_cpu_utilization_high_threshold_percent} for ${var.web_cpu_utilization_high_period_seconds} seconds"
  alarm_actions      = [aws_autoscaling_policy.web_autoscaling_group_scale_up_policy.0.arn]
  
  tags = module.label.tags
  lifecycle {
    ignore_changes = [
        dimensions["AutoScalingGroupName"],
        alarm_actions,
        treat_missing_data,
        tags,
    ]
}
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_low" {
  count               = var.web_asg_enabled ? 1 : 0
  alarm_name          = lower("${module.label.name}-${module.label.environment}-${var.region}-asg-alarm-cpulow-web")
  comparison_operator = var.web_cpu_utilization_low_comparison_operator 
  evaluation_periods  = var.web_cpu_utilization_low_evaluation_periods
  metric_name         = var.web_cpu_metric_name
  namespace           = var.web_cpu_namespace
  period              = var.web_cpu_utilization_low_period_seconds
  statistic           = var.web_cpu_utilization_low_statistic
  threshold           = var.web_cpu_utilization_low_threshold_percent

  dimensions = {
    AutoScalingGroupName = module.web_asg.autoscaling_group_name
      }

  alarm_description = "Alarm if CPU Utilization is below ${var.web_cpu_utilization_low_threshold_percent} for ${var.web_cpu_utilization_low_period_seconds} seconds"
  alarm_actions     = [aws_autoscaling_policy.web_autoscaling_group_scale_down_policy.0.arn]
     
  tags = module.label.tags
  lifecycle {
    ignore_changes = [
        dimensions["AutoScalingGroupName"],
        alarm_actions,
        treat_missing_data,
        tags,
    ]
}
}