### Vault Role Name ###
vault_role_name     = "cfg-shared-services-eds-app-automation-role"

### LABEL MODULE VARS ###
app_name            = "eds"
app_environment     = "PROD"

mandatory_tags = {
    ApplicationID   = "SYSID-06900"
    applicationname = "EDS"
    assignmentgroup = ""    
    PlanviewID      = "PV8060"
    Criticality     = ""
    Requestor       = "james.p.gregg@citizensbank.com"
    Support         = "DL-EnterpriseDataServicesDatabaseAWS@citizensbank.com"
    CostCenter      = ""
    DataClass       = "Internal"
    BusinessMapping = ""
}

additional_tags = {
    "Patch Group"   = ""
    ServerType      = ""
    BackupPlan      = "Prod"
}

# Common:
vpc_id_ingress          = ""
vpc_id_internal         = "vpc-09acec3ce83b37f92"
iam_instance_profile    = "cfg-eds-infrastructure-ec2-us-east-1"
key_name                = "LinuxShrKP"
region                  = "us-east-1"

### APP ALB MODULE VARS ###

app_alb_load_balancer_type  = "application"
app_alb_internal            = "true"

app_alb_access_logs = {
	bucket = "cfg-centralized-elb"
}

app_alb_target_groups = [
    {
        name             = "eds-PROD-us-east-1-tg-app"
        vpc_id           = "vpc-09acec3ce83b37f92"
        backend_protocol = "HTTPS"
        backend_port     = 443
        target_type      = "instance"
        health_check     = {
            enabled             = true
            path                = "/"
            protocol            = "HTTPS"
            matcher             = "200"
            healthy_threshold   = 2
            unhealthy_threshold = 5
            interval            = 10
        }
    }
]	

app_alb_https_listeners = [
     {
        port                = 443
        protocol            = "HTTPS"
        certificate_arn     = "arn:aws:acm:us-east-1:793022030544:certificate/"
        target_group_index  = 0
   }
 ]


### APP ASG MODULE VARS ###

#ASG variables
app_instance_type           = "t3.small"
app_block_device_mappings   = [
  {
    device_name = "/dev/sdg"
    volume_type = "gp2"
    volume_size = "40"
    encrypted   = true
    kms_key_id  = "arn:aws:kms:us-east-1:793022030544:alias/cfg-shared-services-ue1-eds-infrastructure-EBS-nonpci-cmk"
  }
]

app_create_lt                   = "true"
app_create_asg                  = "true"
app_min_size                    = "2"
app_max_size                    = "4"
app_desired_capacity            = "2"
app_bootstrap_file_path         = "templates/app_bootstrap.tpl"
app_health_check_type           = "EC2"
app_health_check_grace_period   = "360"
app_capacity_rebalance          = "true"
app_instance_refresh            = {
  strategy = "Rolling"
  preferences = {
    instance_warmup        = "360"
    min_healthy_percentage = "90"
  }
  triggers = ["tags"]
}
app_mixed_instances_policy      = {
  instances_distribution = {
    on_demand_allocation_strategy              = "prioritized"
    on_demand_base_capacity                    = "1"
    on_demand_percentage_above_base_capacity   = null
    spot_allocation_strategy                   = "capacity-optimized"

    #Only works with lowest price allocation strategy
    spot_instance_pools = null
    #Default: an empty string which means the on-demand price.
    spot_max_price      = null
  }
  override = [{
    instance_type     = "t3.small"
    weighted_capacity = null
    }, {
    instance_type     = "t3.medium"
    weighted_capacity = null
  }]
}

#App Autoscaling Metric Policy Variables
app_scaling_adjustment_type     = "ChangeInCapacity"
app_scaling_policy_type         = "SimpleScaling"
app_scaling_adjustment_add      = 1
app_scaling_adjustment_sub      = -1

app_cpu_metric_name 			= "CPUUtilization"
app_cpu_namespace 			    = "AWS/EC2"

app_cpu_utilization_high_comparison_operator = "GreaterThanOrEqualToThreshold"
app_cpu_utilization_high_statistic           = "Average"
app_cpu_utilization_high_threshold_percent   = 60
app_cpu_utilization_high_evaluation_periods  = 2
app_cpu_utilization_high_period_seconds      = 60
app_cpu_utilization_low_comparison_operator  = "LessThanOrEqualToThreshold"
app_cpu_utilization_low_statistic            = "Average"
app_cpu_utilization_low_threshold_percent    = 25
app_cpu_utilization_low_evaluation_periods   = 10
app_cpu_utilization_low_period_seconds       = 60


### WEB ALB MODULE VARS ###

web_alb_load_balancer_type  = "application"
web_alb_internal            = "true"

web_alb_access_logs = {
	bucket = "cfg-centralized-elb"
}

web_alb_target_groups = [
    {
        name             = "eds-PROD-us-east-1-tg-web"
        vpc_id           = ""
        backend_protocol = "HTTPS"
        backend_port     = 443
        target_type      = "instance"
        health_check     = {
            enabled             = true
            path                = "/"
            protocol            = "HTTPS"
            matcher             = "200"
            healthy_threshold   = 2
            unhealthy_threshold = 5
            interval            = 10
        }
    }
]	

web_alb_https_listeners = [
     {
        port                = 443
        protocol            = "HTTPS"
        certificate_arn     = "arn:aws:acm:us-east-1:793022030544:certificate/"
        target_group_index  = 0
   }
 ]


### WEB ASG MODULE VARS ###

#ASG variables
web_instance_type           = "t3.small"
web_block_device_mappings   = [
  {
    device_name = "/dev/sdg"
    volume_type = "gp2"
    volume_size = "40"
    encrypted   = true
    kms_key_id  = "arn:aws:kms:us-east-1:793022030544:alias/cfg-shared-services-ue1-eds-infrastructure-EBS-nonpci-cmk"
  }
]

web_create_lt                   = "true"
web_create_asg                  = "true"
web_min_size                    = "2"
web_max_size                    = "4"
web_desired_capacity            = "2"
web_bootstrap_file_path         = "templates/web_bootstrap.tpl"
web_health_check_type           = "EC2"
web_health_check_grace_period   = "360"
web_capacity_rebalance          = "true"
web_instance_refresh            = {
  strategy = "Rolling"
  preferences = {
    instance_warmup        = "360"
    min_healthy_percentage = "90"
  }
  triggers = ["tags"]
}
web_mixed_instances_policy      = {
  instances_distribution = {
    on_demand_allocation_strategy              = "prioritized"
    on_demand_base_capacity                    = "1"
    on_demand_percentage_above_base_capacity   = null
    spot_allocation_strategy                   = "capacity-optimized"

    #Only works with lowest price allocation strategy
    spot_instance_pools = null
    #Default: an empty string which means the on-demand price.
    spot_max_price      = null
  }
  override = [{
    instance_type     = "t3.small"
    weighted_capacity = null
    }, {
    instance_type     = "t3.medium"
    weighted_capacity = null
  }]
}

# Web Autoscaling Metric Policy Variables
web_scaling_adjustment_type     = "ChangeInCapacity"
web_scaling_policy_type         = "SimpleScaling"
web_scaling_adjustment_add      = 1
web_scaling_adjustment_sub      = -1

web_cpu_metric_name 			= "CPUUtilization"
web_cpu_namespace 			    = "AWS/EC2"

web_cpu_utilization_high_comparison_operator = "GreaterThanOrEqualToThreshold"
web_cpu_utilization_high_statistic           = "Average"
web_cpu_utilization_high_threshold_percent   = 60
web_cpu_utilization_high_evaluation_periods  = 2
web_cpu_utilization_high_period_seconds      = 60
web_cpu_utilization_low_comparison_operator  = "LessThanOrEqualToThreshold"
web_cpu_utilization_low_statistic            = "Average"
web_cpu_utilization_low_threshold_percent    = 25
web_cpu_utilization_low_evaluation_periods   = 10
web_cpu_utilization_low_period_seconds       = 60