output "web_lb_dns_name" {
    description = "WebLB DNS Name"
    value = module.web_alb.lb_dns_name
}

output "web_asg_id" {
    description = "Web autoscaling group id"
    value       = module.web_asg.autoscaling_group_id
}

output "web_lt_id" {
    description = "Web launch template id"
    value       = module.web_asg.launch_template_id
}