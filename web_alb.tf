module "web_alb" {
  source  = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/alb-module/aws"
  version = "2.0.9"

  load_balancer_type    = var.web_alb_load_balancer_type
  vpc_id                = var.vpc_id_ingress
  subnets               = data.aws_subnet_ids.web_subnets_ingress.ids
  security_groups       = concat(data.aws_security_groups.sg_WebLB.ids, data.aws_security_groups.allow_tls_ingress.ids)
  access_logs           = var.web_alb_access_logs
  target_groups         = var.web_alb_target_groups
  https_listeners       = var.web_alb_https_listeners
  internal              = var.web_alb_internal
  tags                  = module.label.tags
  name                  = lower("${module.label.name}-${module.label.environment}-${var.region}-alb-web")
}