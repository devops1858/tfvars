data "template_file" "app_bootstrap" {
  template = file(var.app_bootstrap_file_path)
}

module "app_asg" {
  source                    = "terraform-cldsvc-prod.corp.internal.citizensbank.com/cfg-cloud-services/autoscaling-module/aws"
  version                   = "2.0.7"
  create_lt                 = var.app_create_lt
  security_groups           = concat(data.aws_security_groups.sg_AppServers.ids, data.aws_security_groups.linux_sg_internal.ids)
  name                      = lower("${module.label.name}-${module.label.environment}-${var.region}-asg-app")
  block_device_mappings     = var.app_block_device_mappings
  iam_instance_profile      = var.iam_instance_profile
  image_id                  = data.aws_ami.linux_ami.id
  ebs_optimized             = var.app_ebs_optimized
  capacity_rebalance        = var.app_capacity_rebalance
  create_asg                = var.app_create_asg
  instance_refresh          = var.app_instance_refresh
  mixed_instances_policy    = var.app_mixed_instances_policy
  min_size                  = var.app_min_size
  max_size                  = var.app_max_size
  desired_capacity          = var.app_desired_capacity
  vpc_zone_identifier       = data.aws_subnet_ids.app_subnets_internal.ids
  target_group_arns         = [module.app_alb.target_group_arns[0]]
  health_check_type         = var.app_health_check_type
  health_check_grace_period = var.app_health_check_grace_period
  tags_as_map               = merge(module.label.tags, {"Name" = "${module.label.name}-${lookup(var.mandatory_tags, "ApplicationID")}-${module.label.environment}-${var.region}-instance-app"})
  instance_type             = var.app_instance_type
  key_name                  = var.key_name
  user_data_base64          = base64encode(data.template_file.app_bootstrap.rendered)
}

resource "aws_autoscaling_lifecycle_hook" "app_hook" {
  count                  = var.app_create_asg ? 1 : 0
  name                   = lower("${module.label.name}-${module.label.environment}-${var.region}-lifecycle-hook")
  autoscaling_group_name = module.app_asg.autoscaling_group_name
  default_result         = var.app_initial_lifecycle_hook_default_result
  lifecycle_transition   = var.app_initial_lifecycle_hook_lifecycle_transition
  heartbeat_timeout      = var.app_initial_lifecycle_hook_heartbeat_timeout
}

resource "aws_autoscaling_policy" "app_autoscaling_group_scale_up_policy" {
  count                   = var.app_asg_enabled ? 1 : 0
  name                    = lower("${module.label.name}-${module.label.environment}-${var.region}-asg-scaleup-policy-app")
  adjustment_type         = var.app_scaling_adjustment_type
  policy_type             = var.app_scaling_policy_type
  scaling_adjustment      = var.app_scaling_adjustment_add
  autoscaling_group_name  = module.app_asg.autoscaling_group_name
}

resource "aws_autoscaling_policy" "app_autoscaling_group_scale_down_policy" {
  count                   = var.app_asg_enabled ? 1 : 0
  name       		          = lower("${module.label.name}-${module.label.environment}-${var.region}-asg-scaledown-policy-app")
  adjustment_type         = var.app_scaling_adjustment_type
  policy_type             = var.app_scaling_policy_type
  scaling_adjustment      = var.app_scaling_adjustment_sub
  autoscaling_group_name  = module.app_asg.autoscaling_group_name
}

resource "aws_cloudwatch_metric_alarm" "app_cpu_high" {
  count               = var.app_asg_enabled ? 1 : 0
  alarm_name          = lower("${module.label.name}-${module.label.environment}-${var.region}-asg-alarm-cpuhigh-app")
  comparison_operator = var.app_cpu_utilization_high_comparison_operator 
  evaluation_periods  = var.app_cpu_utilization_high_evaluation_periods
  metric_name         = var.app_cpu_metric_name 
  namespace           = var.app_cpu_namespace 
  period              = var.app_cpu_utilization_high_period_seconds
  statistic           = var.app_cpu_utilization_high_statistic
  threshold           = var.app_cpu_utilization_high_threshold_percent

  dimensions = {
    AutoScalingGroupName = module.app_asg.autoscaling_group_name
  }

  alarm_description  = "Alarm if CPU Utilization is above ${var.app_cpu_utilization_high_threshold_percent} for ${var.app_cpu_utilization_high_period_seconds} seconds"
  alarm_actions      = [aws_autoscaling_policy.app_autoscaling_group_scale_up_policy.0.arn]
  
  tags = module.label.tags
  lifecycle {
    ignore_changes = [
        dimensions["AutoScalingGroupName"],
        alarm_actions,
        treat_missing_data,
        tags,
    ]
}
}

resource "aws_cloudwatch_metric_alarm" "app_cpu_low" {
  count               = var.app_asg_enabled ? 1 : 0
  alarm_name          = lower("${module.label.name}-${module.label.environment}-${var.region}-asg-alarm-cpulow-app")
  comparison_operator = var.app_cpu_utilization_low_comparison_operator 
  evaluation_periods  = var.app_cpu_utilization_low_evaluation_periods
  metric_name         = var.app_cpu_metric_name
  namespace           = var.app_cpu_namespace
  period              = var.app_cpu_utilization_low_period_seconds
  statistic           = var.app_cpu_utilization_low_statistic
  threshold           = var.app_cpu_utilization_low_threshold_percent

  dimensions = {
    AutoScalingGroupName = module.app_asg.autoscaling_group_name
      }

  alarm_description = "Alarm if CPU Utilization is below ${var.app_cpu_utilization_low_threshold_percent} for ${var.app_cpu_utilization_low_period_seconds} seconds"
  alarm_actions     = [aws_autoscaling_policy.app_autoscaling_group_scale_down_policy.0.arn]
     
  tags = module.label.tags
  lifecycle {
    ignore_changes = [
        dimensions["AutoScalingGroupName"],
        alarm_actions,
        treat_missing_data,
        tags,
    ]
}
}