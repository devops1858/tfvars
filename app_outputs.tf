output "app_lb_dns_name" {
    description = "AppLB DNS Name"
    value = module.app_alb.lb_dns_name
}

output "app_asg_id" {
    description = "App autoscaling group id"
    value       = module.app_asg.autoscaling_group_id
}

output "app_lt_id" {
    description = "App launch template id"
    value       = module.app_asg.launch_template_id
}